const express = require("express");
const bodyParser = require("body-parser");
const responseTime = require("response-time");
const rateLimit = require("express-rate-limit");
const logger = require("morgan");
const path = require("path");
const cors = require("cors");

require("dotenv").config();
require("./config/connection");

let routes = require("./routes/commonRoutes");
let managerRouter = require("./routes/managerRoutes");
let leverageRouter = require("./routes/leverageRoutes");
let symbolgroupRoutes = require("./routes/symbolgroupRoutes");
let orderRoutes = require("./routes/orderRoutes");

const app = express();
const port = process.env.PORT;

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 500, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
  message: "Too many requests, please try again later.",
});

app.use(cors());
app.use(limiter);
app.use(logger("dev"));
app.use(express.json());
app.use(responseTime());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

/* Middleware to log time for each api call */
app.use(responseTime());

app.use(express.static("./public"));
app.set("views", path.join(__dirname, "views"));

app.get("/", (req, res) => {
  res.send("Hi!, welcome to forex servers");
});

app.use("/api/v1", routes);
app.use("/api/v1/manager", managerRouter);
app.use("/api/v1/leverage", leverageRouter);
app.use("/api/v1/groups", symbolgroupRoutes);
app.use("/api/v1/order", orderRoutes);

app.listen(port, () => {
  console.log(`Server Started at ${port}`);
});
