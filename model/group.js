const mongoose = require('mongoose');
const groupSchema = new mongoose.Schema({
  groupName: {
    type: String,
    required: true,
  },
  symbols: {
    type: [String],
    required: true
  },
  members: {
    type: [String],
    required: true
  },
  value_x: {
    type: String,
  },
  spread: {
    type: String,
    required: true,
  },
  commission: {
    type: String
  },
  timeInterval: {
    to: {
      type: String, required: false
    },
    from: {
      type: String, required: false
    }
  },
  status: {
    type: Boolean,
    default: true
  },
  createdAt: {
    type: String
  },
  updatedAt: {
    type: String
  }
});


module.exports = mongoose.model('Group', groupSchema)