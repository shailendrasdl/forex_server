const mongoose = require('mongoose');

const convertcurrenySchema = new mongoose.Schema({
    contract_size: {
        type: String,
    },
    lot_size: {
        type: String,
    },
    currentPrice: {
        type: String,
    },
    openPrice: {
        type: String,
    },
    symbolprice: {
        type: String,
    },
    convertcurreny: {
        type: String,
    },
    EUR: {
        type: String,
    },
    USD: {
        type: String,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        required: true,
    },
})

module.exports = mongoose.model('Convertcurreny', convertcurrenySchema);