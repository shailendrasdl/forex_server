const mongoose = require('mongoose');

const transactionSchema =new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        trim: true
    },
    transactionType: {
        type: String,
        trim: true
    },
    transactionAmount: {
        type: Number,
        default: 0
      },
      accountBalance: {
        type: Number,
        default: 0
      },
      currentBalance: {
        type: Number,
        default: 0
      },
      createdAt: {
        type: String
      },
      updatedAt: {
        type: String
      }
    
}, 
{
    timestamps: true
});

module.exports = mongoose.model('Transaction', transactionSchema)