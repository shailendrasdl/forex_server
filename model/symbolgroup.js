const mongoose = require('mongoose');
const symbolgroupSchema = new mongoose.Schema({
    close_time: {
        type: String,
    },
    group_symbol: {
        type: String,
    },
    trade_time: {
        type: String,
    },
    both_quotes: {
        type: String,
    },
    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        required: true,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },

},
    { timestamps: true }
)

module.exports = mongoose.model('Symbolgroup', symbolgroupSchema);