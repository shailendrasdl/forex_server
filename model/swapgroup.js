const mongoose = require('mongoose');
const swapgroupSchema = new mongoose.Schema({
    group_symbol: {
        type: String,
    },
    symbol_name: {
        type: String,
    },
    Swap1: {
        type: String,
    },
    Swap2: {
        type: String,
    },
    Swap3: {
        type: String,
    },
    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        required: true,
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },

},
    { timestamps: true }
)

module.exports = mongoose.model('Swapgroup', swapgroupSchema);