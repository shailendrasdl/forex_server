const mongoose = require('mongoose');
const companySchema = new mongoose.Schema({
  companyName: {
    required: true,
    type: String
  },
  companyImage: {
    type: String,
    required: false,
  },
  status: {
    type: Boolean,
    required: true,
  },
  createdAt: {
    type: String
  },
  updatedAt: {
    type: String
  }
})

module.exports = mongoose.model('Company', companySchema)
