const mongoose = require('mongoose');

const leverageSchema = new mongoose.Schema({
    symbol: {
        type: String,
    },
    leveragetype: {
        type: String,
    },
    avail_balance: {
        type: String,
    },
    leverage: {
        type: String,
    },
    Margin_Balance: {
        type: String,
    },
    required_margin: {
        type: String,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
})

module.exports = mongoose.model('Leverage', leverageSchema);