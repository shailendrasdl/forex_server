const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const orderSchema = new Schema(
  {
    symbol: {
      type: String,
    },
    volume: {
      type: String,
    },
    stop_loss: {
      type: String,
    },
    take_profit: {
      type: String,
    },
    comment: {
      type: String,
    },
    order_type: {
      type: String,
    },
    panding_order: {
      type: {
        type: String,
      },
      at_price: {
        type: String,
      },
      expiry: {
        type: String,
      },
    },
    status: {
      type: String,
      default: "inactive",
      enum: ["inactive", "active"],
    },
    userid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    adminid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Admin",
      // required: true,
    },
    currentPrice: {
      type: String,
    },
  },
  { collection: "Order", strict: false, timestamps: { createdAt: "createdAt" } }
);

module.exports = mongoose.model("Order", orderSchema);
