const mongoose = require('mongoose');
const adminSchema = new mongoose.Schema({
  userName: { required: true, type: String, trim: true },
  email: { type: String, required: true, unique: true, trim: true },
  password: { type: String, required: true, trim: true },
  description:{type: String},
  role: { type: [String] },
  permission: {
    givingbonus: { type: Boolean },
    changingleverage: { type: Boolean },
    changinggroup: { type: Boolean },
    changingspreadcalculation: { type: Boolean },
    changingpasswordofclient: { type: Boolean },
    changinggroupswapgrouporsymbol: { type: Boolean },
    companygroup: { type: Boolean },
    clientinformations: { type: Boolean },
    addremovedeposit: { type: Boolean },
  },
  mobileNumber: { type: String, maxlength: 10, unique: true },
  createdAt: { type: String },
  updatedAt: { type: String }
})

module.exports = mongoose.model('Admin', adminSchema)