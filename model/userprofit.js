const mongoose = require('mongoose');

const userprofitSchema = new mongoose.Schema({
    contract_size: {
        type: String,
    },
    lot_size: {
        type: String,
    },
    price: {
        type: String,
    },
    leverage: {
        type: String,
    },
    pips: {
        type: String,
    },
    currentPrice: {
        type: String,
    },
    openPrice: {
        type: String,
    },
    Account_currency: {
        type: String,
    },
    pipvalue: {
        type: String,
    },
    buy_profit_calculation: {
        type: String,
    },
    sell_profit_calculation: {
        type: String,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        required: true,
    }
})

module.exports = mongoose.model('User_profit', userprofitSchema);