const mongoose = require('mongoose');

const CompanygroupSchema = new mongoose.Schema({
    Symbol_Group: {
        type: String,
    },
    Swap_Group: {
        type: String,
    },
    Company_name: {
        type: String,
    },
    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        required: true,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },

},
    { timestamps: true }
)

module.exports = mongoose.model('Companygroup', CompanygroupSchema);