const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
  title: {
    type: String
  },
  firstName: {
    required: true,
    type: String,
    trim: true,
  },
  lastName: {
    required: true,
    type: String,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  mobileNumber: {
    type: String,
    maxlength: 10,
    required: true,
  },
  imageUrl: {
    type: String,
  },
  country: {
    type: String,
  },
  state: {
    type: String
  },
  address: {
    type: String,
  },
  postCode: {
    type: String,
  },
  planType: {
    type: String
  },
  accountCurrency: {
    type: String
  },
  riskMode: {
    type: String
  },
  accountBalance: {
    type: Number,
    default: 0
  },
  authToken: {
    type: String,
  },
  language: {
    type: String,
  },
  timeZone: {
    type: String,
  },
  is_active: {
    type: Boolean,
    default: false,
  },
  status: {
    type: Boolean,
    default: true
  },
  credentials: {
    loginId: {
      type: String, required: false
    },
    password: {
      type: String, required: false
    },
    server: {
      type: String, required: false
    },
    platformWindows: {
      type: String, required: false
    },
  },
  createdAt: {
    type: String
  },
  updatedAt: {
    type: String
  }
})

module.exports = mongoose.model('User', userSchema)