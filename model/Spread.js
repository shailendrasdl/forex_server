const mongoose = require('mongoose');

const SpreadSchema = new mongoose.Schema({
    bidvalue: {
        type: String,
    },
    askvalue: {
        type: String,
    },
    xvalue: {
        type: String,
    },
    Askplatform: {
        type: String,
    },
    bidplatform: {
        type: String,
    },
    Spread: {
        type: String,
    },
    status: {
        type: String,
        default: "inactive",
        enum: ["inactive", "active"],
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        required: true,
    },
})

module.exports = mongoose.model('Spread', SpreadSchema);