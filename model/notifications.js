const mongoose = require('mongoose');

const notificationSchema =new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        trim: true
    },
    transactionType: {
        type: String,
        trim: true
    },
    transactionAmount: {
        type: Number,
        default: 0
      },
      type: {
        type: String
      },
      processed: {
        type: Number,
        default:0
      },
      createdAt: {
        type: String
      },
      updatedAt: {
        type: String
      }
    
}, 
{
    timestamps: true
});

module.exports = mongoose.model('Notification', notificationSchema)