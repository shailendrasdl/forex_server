var express = require('express');
var router = express.Router();

const managerValidation = require('../utils/manager.validator');

const adminController = require('../controllers/adminController');
const isLoggedIn = require('../middleware/auth');

router.post('/adminSignup',managerValidation.signUp, adminController.adminSignup);
router.post('/adminLogin', managerValidation.login,  adminController.adminLogin);
router.post('/adminDetailsById', isLoggedIn, adminController.adminDetailById);
router.post('/changePassword', isLoggedIn, adminController.changePassword)

router.post('/createCompany', managerValidation.createCompany,adminController.createCompany);
router.get('/getCompanyList', adminController.getCompanyList);
router.post('/getCompanyDetails', adminController.getCompanyDetails);

router.get('/forexExchanges', isLoggedIn, adminController.forexExchanges);
router.get('/getSymbolsList', isLoggedIn, adminController.symbolsList);
router.get('/forexSymbol', isLoggedIn, adminController.forexSymbol);
router.post('/createGroup', isLoggedIn, managerValidation.createGroup, adminController.createGroup );
router.post('/updateGroup', isLoggedIn, adminController.updateGroupController);
router.post('/addSubAdmin', isLoggedIn, managerValidation.createSubAdmin, adminController.addSubAdmin);
router.get('/orderList', isLoggedIn, adminController.getOrderList);
router.post('/updateOrder', isLoggedIn, adminController.updateOrderDetail)

/*---------------------------- Gafiq Work Start -----------------------------------------*/
router.get('/groupList', isLoggedIn,  adminController.getGroupList);
router.get('/groupDetails', isLoggedIn,  adminController.getGroupDetails);
router.delete('/deleteGroup', isLoggedIn, adminController.deleteGroup );
router.get('/subAdminList', isLoggedIn, adminController.subAdminList );
router.get('/subAdminDetails', isLoggedIn, adminController.subAdminDetails ); // not use
router.post('/updateSubAdmin', adminController.updateSubAdmin );
router.delete('/deleteSubAdmin', adminController.deleteSubAdmin );
/*---------------------------- Gafiq Work End --------------------------------------------*/


module.exports = router;