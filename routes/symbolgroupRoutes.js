var express = require('express');

var router = express.Router();

const auth = require('../middleware/auth');

const symbolgroupController = require('../controllers/symbolgroupController');

//-------------------------------------------------- Group Symbol -----------------------------------------------------//

router.post('/Group_symbol/:id', symbolgroupController.Groupsymbol);

router.get('/Singlegroupsymbol/:id', symbolgroupController.singlegroupsymbol);

router.delete('/Deletegroupsymbol/:id', symbolgroupController.deletegroupsymbol);

router.put('/editgroupsymbol/:id', symbolgroupController.editGroupsymbol);

router.get('/viewgroupsymbol', symbolgroupController.viewgroupsymbol);

//-------------------------------------------------- Company Group -----------------------------------------------------//

router.post('/Companygroup/:id', symbolgroupController.CompanyGroup);

router.get('/Singlecompany/:id', symbolgroupController.singlecompanygroup);

router.put('/editcompanygroup/:id', symbolgroupController.editCompanygroup);

router.delete('/Deletecompany/:id', symbolgroupController.deletecompanygroup);

router.get('/Viewcompany', symbolgroupController.viewcompanygroup);

//-------------------------------------------------- Swap Group -----------------------------------------------------//

router.post('/Swapgroup/:userid/:adminid', symbolgroupController.Swapgroup);

router.get('/Singleswap/:id', symbolgroupController.singleswapgroup);

router.put('/editswapgroup/:id', symbolgroupController.editSwapgroup);

router.delete('/Deleteswap/:id', symbolgroupController.deleteswapgroup);

router.get('/Viewswap', symbolgroupController.viewswapgroup);

module.exports = router;