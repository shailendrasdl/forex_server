var express = require("express");

var router = express.Router();

const auth = require("../middleware/auth");

const orderController = require("../controllers/orderController");
const { route } = require("./commonRoutes");

// Buy Limit
router.post("/buy/limit", orderController.buyLimit);
router.put("/buy/limit/:id", orderController.modifyBuyLimit);
router.delete("/buy/limit/:id", orderController.deleteBuyLimit);

// Buy Stop
router.post("/buy/buyStop", orderController.buyStop);
router.put("/buy/buyStop/:id", orderController.modifyBuyStop);
router.delete("/buy/buyStop/:id", orderController.deleteBuyStop);

// Sell Limit
router.post("/sell/limit", orderController.sellLimit);
router.put("/sell/limit/:id", orderController.modifySellLimit);
router.delete("/sell/limit/:id", orderController.deleteSellLimit);

// Sell Stop
router.post("/sell/sellStop", orderController.sellStop);
router.put("/sell/sellStop/:id", orderController.modifySellStop);
router.delete("/sell/sellStop/:id", orderController.deleteSellStop);

// Execution of pending orders
router.put("/open/:id", orderController.openOrder);
router.put("/close/:id", orderController.closeOrder);

/*---------------------------- currency pair USD not the firstly written one --------------------------------------------*/

router.post("/profit/buy/open", orderController.buyOpenOrder);

router.post("/profit/buy/closed", orderController.buyClosedOrder);

router.post("/profit/sell/open", orderController.sellOpenOrder);

router.post("/profit/sell/closed", orderController.sellClosedOrder);

/*---------------------------- currency pair USD is the firstly written one --------------------------------------------*/

router.post(
  "/profit/buy/open/first",
  orderController.firstCurrencyBuyOpenOrder
);

router.post(
  "/profit/buy/closed/first",
  orderController.firstCurrencyBuyClosedOrder
);

router.post(
  "/profit/sell/open/first",
  orderController.firstCurrencySellOpenOrder
);

router.post(
  "/profit/sell/closed/first",
  orderController.firstCurrencySellClosedOrder
);

/*---------------------------- currency pair does not include USD --------------------------------------------*/

router.post(
  "/profit/buy/open/without",
  orderController.withoutCurrencyBuyOpenOrder
);

router.post(
  "/profit/buy/closed/without",
  orderController.withoutCurrencyBuyClosedOrder
);

router.post(
  "/profit/sell/open/without",
  orderController.withoutCurrencySellOpenOrder
);

router.post(
  "/profit/sell/closed/without",
  orderController.withoutCurrencySellClosedOrder
);

/*---------------------------- currency pair not the firstly written one --------------------------------------------*/

router.post("/profit/buy/open/dax", orderController.daxBuyOpenOrder);

// router.post("/profit/buy/open/us30", orderController.us30buyOpenOrder);

// router.post("/profit/sell/open/us100", orderController.us100buyOpenOrder);

// router.post("/profit/sell/open/us500", orderController.us500buyOpenOrder);

// router.post("/profit/sell/open/oil", orderController.oilBuyOpenOrder);

// ALL Orders By userId
router.get("/orderAll/:id", orderController.orderByUser);

module.exports = router;
