var express = require('express');

var router = express.Router();

const auth = require('../middleware/auth');

const leverageController = require('../controllers/leverageController');

router.post('/leverage/:userid', leverageController.leverageExchanges);

router.put('/editleverage/:id', leverageController.editleverage);

router.post('/Spread/:userid/:adminid', leverageController.Spread);

router.put('/editspread/:id', leverageController.editSpread);

router.post('/user_profit/:userid/:adminid', leverageController.user_profit);

router.put('/edituserprofit/:id', leverageController.editUserprofit);

router.post('/curreny/:userid/:adminid', leverageController.Convertcurreny);

router.put('/editcurreny/:id', leverageController.editCurreny);

router.post('/first_Swap', leverageController.Swap1);

router.post('/second_Swap', leverageController.Swap2);

module.exports = router;