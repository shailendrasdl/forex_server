const express = require('express');
const session = require('express-session');

const router = express.Router()

const userValidation = require('../utils/user.validator');
const isLoggedIn = require('../middleware/auth');
const commonController = require('../controllers/commonController');
const userController = require('../controllers/userController');
const user = require('../model/user');

router.use(
  session({
    secret: 'somerandomforex',
    resave: false,
    saveUninitialized: false,
    cookie: { expires: 6000000 }
  })
);

router.post('/signup', userValidation.signup, userController.signupUser);
router.post('/login', userValidation.login, userController.loginUser);
router.post('/forgotPassword', userValidation.forgotPassword, userController.forgotPassword);
router.put('/changePassword', isLoggedIn, userController.changePassword);
router.get('/userDetailsById', isLoggedIn, userValidation.userDetailsById, userController.getUserDetailsById);
router.get('/userList', isLoggedIn, userController.getUserList);
router.get('/getActiveUser', isLoggedIn, userController.activeUser);
router.get('/getInactiveUser', isLoggedIn, userController.inactiveUser);

router.put('/updateProfile', isLoggedIn, userValidation.updateProfile, userController.updateProfile);
router.post('/contact', isLoggedIn, userValidation.contact, userController.contactUs);
router.get('/searchUser', isLoggedIn, userController.searchUser);

router.post('/logout', userController.logout);

router.put('/selectPlan', isLoggedIn, userController.selectPlanDetails);
router.put('/addToCurrencyType', isLoggedIn, userController.accountCurrency);
router.put('/addRiskMode', isLoggedIn, userController.riskMode);
router.put('/addToBalance', isLoggedIn, userController.addAccountBalance);
router.get('/download', userController.downloadSetup)

/*---------------------------- Gafiq Work Start --------------------------------------------*/
router.get('/getUserBalance', userController.getUserBalance);
router.put('/deductAccountBalance', userController.deductAccountBalance);

router.post('/generateNotification',isLoggedIn, userController.generateNotification);
router.get('/getNotificationList',isLoggedIn, userController.getNotificationList);
router.get('/getNotificationListByUser',isLoggedIn, userController.getPendingNotificationListByUser);
router.get('/getPendingNotificationListByUser',isLoggedIn, userController.getPendingNotificationListByUser);
router.get('/getProcessedNotificationListByUser',isLoggedIn, userController.getProcessedNotificationListByUser);
router.post('/approveNotification',isLoggedIn, userController.approveNotification);
/*---------------------------- Gafiq Work End --------------------------------------------*/



module.exports = router;
