const moment = require('moment');
const groupModel = require('../model/group');
const companyModel = require('../model/company');
const orderModel = require('../model/order');
const adminModel = require('../model/admin');

const securePassword = require('../utils/securePassword')


exports.UPDATE_PASSWORD = async (data) => {
  const hashedPassword = await securePassword(data.password);
  let post = await adminModel.updateOne({ _id: data.adminId }, {
    $set: { password: hashedPassword, updatedAt: new Date().getTime() }
  });
  return post;
};

exports.updateGroupDetails = async (data) => {
  let post = await groupModel.updateOne({ _id: data.groupId },
    {
      $set: {
        groupName: data.groupName,
        symbols: data.symbols,
        members: data.members,
        spread: data.spread,
        value_x: data.value_x,
        commission: data.commission,
        timeInterval: data.timeInterval,
        updatedAt: new Date().getTime()
      }
    });
  return post;
};
exports.searchCompany = async (companyName) => {
  try {
    const companyData = await companyModel.findOne({ companyName: companyName });
    return companyData;
  } catch (error) {
    console.log('error')
    return false;
  }
}

exports.saveCompanyDetails = async (companyData) => {
  try {
    let createdAt = moment().format("ll");
    let status = true
    companyDetails = { status, createdAt, ...companyData }
    let companyDetailsObj = new companyModel(companyDetails);
    await companyDetailsObj.save();
    return companyDetailsObj;
  } catch (error) {
    console.log('error :', error)
    return false;
  }
}

exports.getCompanyDetails = async () => {
  try {
    const output = await companyModel.find({ status: true }).sort({ "updatedAt": -1 });
    return output;
  } catch (e) {
    return false;
    //console.error(e);
  }
};

exports.orderList = async () => {
  try {
    const output = await orderModel.find().sort({ "updatedAt": -1 });
    return output;
  } catch (e) {
    return false;
    //console.error(e);
  }
};

exports.getOrderDetails = async () => {
  try {
    const output = await orderModel.findOne();
    return output;
  } catch (e) {
    return false;
    //console.error(e);
  }
};



