const userSchema = require('../model/user');
const transactionSchema = require('../model/transaction')
var generator = require('generate-password');


exports.userDetails = async (email) => {
  try {
    let user = await userSchema.findOne({ email: email }).exec();
    return user;
  } catch (error) {
    return false;
  }
}

exports.generatePassword = async () => {
  var password = generator.generate({
    length: 8,
    numbers: true,
    uppercase: false
  });
  return password
}

exports.updatePassword = async (email, hashedPassword) => {
  try {
    const output = await userSchema.updateOne({ email: email }, { $set: { password: hashedPassword } });
    return output
  } catch (error) {
    return false;
  }
};

exports.selectPlanDetails = async (userId, planType) => {
  try {
    const output = await userSchema.updateOne({ _id: userId }, {
      $set: {
        planType: planType, updatedAt: new Date().getTime()
      }
    });
    return true
  } catch (error) {
    return false;
  }
};

exports.addAccountCurrency = async (userId, currency) => {
  try {
    const output = await userSchema.updateOne({ _id: userId }, {
      $set: {
        accountCurrency: currency, updatedAt: new Date().getTime()
      }
    });
    return true
  } catch (error) {
    return false;
  }
};
exports.addriskMode = async (userId, mode) => {
  try {
    const output = await userSchema.updateOne({ _id: userId }, {
      $set: {
        riskMode: mode, updatedAt: new Date().getTime()
      }
    });
    return true
  } catch (error) {
    return false;
  }
};


/*
  exports.addToBalance = async (userId, amount) => {
    try {
      const output = await userSchema.updateOne({_id: userId}, {
        $set: {
          accountBalance: amount, updatedAt : new Date().getTime()
        }
      });
      return true
    } catch (error) {
      return false;
    }
};


exports.addToBalance = async (uId, amount, tranType, curBalance, tranAmount) => {
  try {
    let isSucess = false;

    const output = await userSchema.updateOne({ _id: uId }, {
      $set: {
        accountBalance: amount, updatedAt: new Date().getTime()
      }
    }).then(result => {
      const transaction = transactionSchema.create({
        userId: uId,
        transactionType: tranType,
        transactionAmount: tranAmount,
        accountBalance: curBalance,
        currentBalance: curBalance + tranAmount,
        createdAt: new Date().getTime()
      });
      isSucess = true
    })
      .catch(err => {
        isSucess = false
      });

    return isSucess

  } catch (error) {
    return false;
  }
};

*/
exports.addToBalance = async (uId, amount, tranType, curBalance, tranAmount) => {
  try {
    let isSucess = false;
    const output = await userSchema.updateOne({ _id: uId }, {
      $set: {
        accountBalance: amount, updatedAt: new Date().getTime()
      }
    }).then(result => {
      const transaction = transactionSchema.create({
        userId: uId,
        transactionType: tranType,
        transactionAmount: tranAmount,
        accountBalance: curBalance,
        currentBalance: curBalance + tranAmount,
        createdAt: new Date().getTime()
      });
      isSucess = true
    })
      .catch(err => {
        console.log(err);
        isSucess = false
      });
    return isSucess
  } catch (error) {
    return false;
  }
};

/* add services for Gafiq */
exports.deductAccountBalance = async (userId, amount, tranType, curBalance, tranAmount) => {
  try {
    let isSucess = false;
    await userSchema.updateOne({ _id: userId }, {
      $set: {
        accountBalance: amount, updatedAt: new Date().getTime()
      },
    }).then(result => {
      const transaction = transactionSchema.create({
        userId: userId,
        transactionType: tranType,
        transactionAmount: -tranAmount,
        accountBalance: curBalance,
        currentBalance: curBalance - tranAmount,
        createdAt: new Date().getTime()
      });
      isSucess = true
    })
      .catch(err => {
        isSucess = false
      });
    return isSucess
  } catch (error) {
    console.log(error);
    return false;
  }
};



/*
exports.deductAccountBalance = async (userId, amount, tranType, curBalance, tranAmount) => {
  try {
    let isSucess = false;
    await userSchema.updateOne({ _id: userId }, {
      $set: {
        accountBalance: amount, updatedAt: new Date().getTime()
      },
    }).then(result => {
      console.log('true');
      const transaction = transactionSchema.create({
        userId: userId,
        transactionType: tranType,
        transactionAmount: -tranAmount,
        accountBalance: curBalance,
        currentBalance: curBalance - tranAmount,
        createdAt: new Date().getTime()
      });
      isSucess = true
    })
      .catch(err => {
        isSucess = false
      });
    return isSucess
  } catch (error) {
    console.log(error);
    return false;
  }
};





// change in 3 Time
exports.deductAccountBalance = async (userId, amount, tranType, curBalance, tranAmount) => {
  try {
    const output = await userSchema.updateOne({ _id: userId }, {
      $set: {
        accountBalance: amount, updatedAt: new Date().getTime()
      }
    });
    const transaction = await transactionSchema.create({
      userId: userId,
      transactionType: tranType,
      transactionAmount: -tranAmount,
      accountBalance: curBalance,
      currentBalance: curBalance - tranAmount,
      createdAt: new Date().getTime()
    });
    return true
  } catch (error) {
    return false;
  }
};
*/

/*
// change in 2 Time
exports.deductAccountBalance = async (userId, amount) => {
  try {
    const output = await userSchema.updateOne({_id: userId},{
      $set: {
        accountBalance: amount, updatedAt : new Date().getTime()
      }
    });
    return true
  } catch (error){
    return false;
  }
};
*/