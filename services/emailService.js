const AWS = require("aws-sdk");
const env = require("dotenv");
const User = require('../model/user');
const Notification = require('../model/notifications');

env.config();
 
const awsConfig = {
  accessKeyId: process.env.AccessKey,
  secretAccessKey: process.env.SecretKey,
  region: process.env.region
};
const SES = new AWS.SES(awsConfig);
exports.sendNotificationAdminEmail = (userId , notificationId) =>{

const user= User.findOne({ _id : userId});
const notification= Notification.findOne({ _id : notificationId});
const msgType= "A request for " + notification.transactionType  +" for an amount " + notification.transactionAmount + " has been recieved";
    console.log('testing')
    
  const email =  "mohdrafiq.rafiq3@gmail.com";
    const aws_email = process.env.FROM_EMAIL;
    try {
      var params = {
        Source: aws_email,
        Destination: {
          ToAddresses: [email],
        },
        Message: {
          Subject: {
            Charset: "UTF-8",
            Data: 'Deposit / Withdrawl Request'
          },
          Body: {
            Html: {
              Charset: "UTF-8",
              Data: msgType
            },
          },
        },
      };
      let sendPromise = SES.sendEmail(params).promise();
      console.log('port');
      sendPromise.then((data) => {
        if (data.MessageId) {
          return data.MessageId;
        }
      }).catch((err) => {
        console.log('error',err)
        return false
      })
    } catch (error) {
      console.log(error)
    }

  
  //}
};
  
  exports.sendNotificationUserEmail = (userId , notificationId) =>{
  
    const user= User.findOne({ _id : userId});
    const notification= Notification.findOne({ _id : notificationId});
    const msgType= "A request for " + notification.transactionType  +" for an amount " + notification.transactionAmount + " has been approved";
        
        
      const email = user.email;
        const aws_email = process.env.FROM_EMAIL;
        try {
          var params = {
            Source: aws_email,
            Destination: {
              ToAddresses: [email],
            },
            Message: {
              Subject: {
                Charset: "UTF-8",
                Data: 'Deposit / Withdrawl Request'
              },
              Body: {
                Html: {
                  Charset: "UTF-8",
                  Data: msgType
                },
              },
            },
          };
          let sendPromise = SES.sendEmail(params).promise();
          sendPromise.then((data) => {
            if (data.MessageId) {
              return data.MessageId;
            }
          }).catch((err) => {
            console.log(err)
            return false
          })
        } catch (error) {
          console.log(error)
        }
    
  
  };