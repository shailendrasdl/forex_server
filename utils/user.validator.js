const errorFunction = require('../utils/errorFunction');
const {userSignUp, adminLogin, emailValidator, userIdValidator, contactValidator} = require('./validator');

const signup = async (req, res, next) => {
  const payload = {
    title: req.body.title,
    firstName: req.body.firstName,
    email: req.body.email,
    password: req.body.password,
    mobileNumber: req.body.mobileNumber,
    country: req.body.country,
    address: req.body.address,
    is_active: true,
  };

  const { error } = userSignUp.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in User Data : ${error.message}`)
    );
  } else {
    next();
  }
};

const login = async (req, res, next) => {
  const payload = {
    email: req.body.email,
    password: req.body.password,
  };

  const { error } = adminLogin.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in Login Data : ${error.message}`)
    );
  } else {
    next();
  }
};

const forgotPassword = async (req, res, next) => {
  const payload = {
    email: req.body.email,
  };
  const { error } = emailValidator.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in forgot password data : ${error.message}`)
    );
  } else {
    next();
  }
};

const userDetailsById = async (req, res, next) => {
  const payload = {
    userId: req.query.userId,
  };
  const { error } = userIdValidator.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in user Details By Id data : ${error.message}`)
    );
  } else {
    next();
  }
};

const updateProfile = async (req, res, next) => {
  const payload = {
    userId: req.body.userId,
  };
  const { error } = userIdValidator.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in user profile data : ${error.message}`)
    );
  } else {
    next();
  }
};

const contact = async (req, res, next) => {
  const payload = {
    userId: req.body.userId,
    subject: req.body.subject,
    content: req.body.content,
  };
  const { error } = contactValidator.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in contact data : ${error.message}`)
    );
  } else {
    next();
  }
};

module.exports = {signup, login, forgotPassword, userDetailsById, updateProfile, contact};