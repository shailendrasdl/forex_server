const moment = require('moment');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
var request = require('request');
const finnhub = require('finnhub');
let fs = require('fs');

const Admin = require('../model/admin');
const Group = require('../model/group');
const orderModel = require('../model/order')

const errorFunction = require('../utils/errorFunction');
const securePassword = require('../utils/securePassword')
const adminServices = require('../services/adminServices');

exports.adminSignup = async (req, res, next) => {
  try {
    const post = new Admin();
    post.userName = req.body.userName;
    post.email = req.body.email;
    post.password = req.body.password;
    post.mobileNumber = req.body.mobileNumber;
    post.description = req.body.description;
    post.createdAt = moment().format("ll");
    post.updatedAt = moment().format("ll");
    await post.save();
    res.send(post)
  } catch (error) {
    next(error);
  }
};

exports.adminLogin = async (req, res, next) => {
  const { email, password } = req.body
  if (!email || !password || email == "" || password == "") {
    return res.status(422).json({ success: false, message: "Please add all fields" });
  }
  const admin = await Admin.findOne({ email });
  if (admin && password == admin.password) {

    const token = await jwt.sign({ email: email }, process.env.JWT_SECRET_KEY, {
      expiresIn: process.env.JWT_TOKEN_EXPIRED
    });

    res.status(200);
    return res.json(errorFunction(false, "Admin Login Successfully", { authToken: token, adminId: admin._id  }));
    //return res.json(errorFunction(false, "Admin Login Successfully", { authToken: token }));
  } else {
    res.status(404);
    return res.json(errorFunction(true, "Invalid credentials"));
  }
};


exports.adminDetailById = async (req, res, next) => {
  try {
    if (!req.body.adminId || req.body.adminId == "") {
      res.status(403);
      return res.json(errorFunction(true, "adminId is required"));
    }
    const adminDetails = await Admin.findOne({_id: req.body.adminId});
    if (!adminDetails) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown Admin"))
    } else {
      return res.status(200).json(errorFunction(false, "admin details", adminDetails));
    }
  } catch (error) {
    next(error)
  }
};

exports.changePassword = async (req, res, next) => {
  let data = req.body;
  if (!req.body.adminId || req.body.adminId == "") {
    res.status(403);
    return res.json(errorFunction(true, "adminId is required"));
  }
  try {
    const adminDetails = await Admin.findOne({_id: req.body.adminId});
    if (!adminDetails) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown Admin"))
    } else {
      let output = await adminServices.UPDATE_PASSWORD(data);
      return res.status(200).json(errorFunction(false, "password change successfully"));
    }
  } catch (error) {
    next(error)
  }
};



exports.symbolsList = async (req, res, next) => {
  try {
    await request.get('https://fcsapi.com/api-v3/forex/list?type=forex&access_key=8MZxXMuVuatbI8d379zR7gggh', function (err, response, body) {
      var symbols = JSON.parse(body);
      if (!err && symbols.code == 200) {
        return res.status(200).json(errorFunction(false, "Symbols  list", symbols.response))
      }
    })
  } catch (error) {
    next(error);
  }
};

exports.forexExchanges = async (req, res, next) => {
  const api_key = finnhub.ApiClient.instance.authentications['api_key'];
  api_key.apiKey = "cegovaaad3i0qis38q40cegovaaad3i0qis38q4g"
  const finnhubClient = new finnhub.DefaultApi()

  finnhubClient.forexExchanges((error, data, response) => {
    return res.status(200).json(errorFunction(false, "Symbols  list", data))
  });
}

exports.forexSymbol = async (req, res, next) => {
  const api_key = finnhub.ApiClient.instance.authentications['api_key'];
  api_key.apiKey = "cegovaaad3i0qis38q40cegovaaad3i0qis38q4g"
  const finnhubClient = new finnhub.DefaultApi()

  finnhubClient.forexSymbols("OANDA", (error, data, response) => {
    return res.status(200).json(errorFunction(false, " forex Symbols  list", data))
  });
};


exports.createCompany = async (req, res, next) => {
  if(req.body.companyName == undefined || req.body.companyName == null){
    return res.status(205).json(errorFunction(true, "Malformed request!"));
  }
  const companyData = req.body;
  const company = await adminServices.searchCompany(req.body.companyName);
  if (company) {
    return res.status(200).json(errorFunction(false, "Company already exists!"));
  } else {
    let output = await adminServices.saveCompanyDetails(companyData);
    res.status(200);
    return res.json(errorFunction(false, "Company created Successfully"));
  }
};
exports.getCompanyList = async (req, res, next) => {
  try {
    adminServices.getCompanyDetails().then((response) => {
      return res.status(200).json(errorFunction(false, "company list", response))
    }).catch((error) =>{
      next(error);
    })
  } catch (error) {
    next(error);
  }
}

exports.getCompanyDetails = async (req, res, next) => {
  try {
    const company = await adminServices.searchCompany(req.body.companyName);
    if(company){
      return res.status(200).json(errorFunction(false, "company details", company));
    }
    res.status(403);
    return res.json(errorFunction(true, "Unknown company"));
  } catch (error) {
    next(error)
  }
}


exports.createGroup = async (req, res, next) => {
  const { groupName, symbols, members, spread, value_x, commission, timeInterval } = req.body
  try {
    const createdAt = moment().format("ll")
    const updatedAt =  new Date().getTime()
    const newGroup = new Group({ groupName, symbols, members, spread, value_x, commission, timeInterval, createdAt, updatedAt });
    await newGroup.save().then((response) => {
      res.status(201);
      return res.json(errorFunction(false, "Group Created", response));
    })
  } catch (error) {
    res.status(403);
    return res.json(errorFunction(true, "Something went wrong!")); 
  }
};
exports.updateGroupController = async (req, res, next) => {
  const data = req.body;
  try {
    await adminServices.updateGroupDetails(data).then((response) => {
      if(response) {
        console.log('response :', response)
        return res.status(200).json(errorFunction(
          false, "Group details updated successfully"
        ))
      }
    })
  } catch (error) {
    console.log('error :', error)
    res.status(403);
    return res.json(errorFunction(true, "Something went wrong!"));
  }
}

exports.addSubAdmin = async (req, res, next) => {
  const { userName, email, role, description, mobileNumber, permission } = req.body
  try {
    const subAdmin = await Admin.findOne({email})
    if (subAdmin) {
      res.status(403);
      return res.json(errorFunction(true, "sub admin already exists"));
    } else {
      const hashedPassword = await securePassword(req.body.password);
      const createdAt = moment().format("ll")
      const updatedAt =  new Date().getTime()
      const newSubAdmin = new Admin({ userName, email, role, hashedPassword, description, mobileNumber, permission, createdAt, updatedAt });
      await newSubAdmin.save().then((response) => {
        res.status(201);
        return res.json(errorFunction(false, "sub admin added successfully", response));
      })
    }
  } catch (error) {
    res.status(404);
    console.log(error);
    return res.json(errorFunction(true, "Something went wrong!"));
    next(error)
  }
};


exports.getOrderList = async (req, res, next) => {
  try {
    const order = await adminServices.orderList();
    if(order.length > 0) {
      return res.status(200).json(errorFunction(false, "order list", order));
    }
    else{
      return res.status(404).json(errorFunction(false, "data not found"));
    }
  } catch (error) {
    next(error)
  }
}

exports.updateOrderDetail = async (req, res, next) => {
  const orderDetails = await adminServices.getOrderDetails();
  if(orderDetails) {
    let post = await orderModel.updateOne({_id: req.body.orderId }, {
      $set: req.body
    })
    return res.status(200).json(errorFunction(false, "order details updated successfully"))
  } else {
    return res.status(404).json(errorFunction(false, "order details not found"));
  }
}


/*---------------------------- Gafiq Work Start --------------------------------------------*/
exports.getGroupList = async (req, res, next) => {
  try {
    const groupList = await Group.find();
    if (groupList.length > 0) {
      return res.status(200).json(errorFunction(false, "Group list", groupList))
    } else {
      return res.json(errorFunction(true, "Group list not found"));
    }
  } catch (error) {
    next(error);
  }
};

exports.deleteGroup = async (req, res, next) => {
  try {
    const groupDetail = await Group.findOne({ _id: req.query.groupId }).exec();
    if (groupDetail) {
      const id = await Group.deleteOne({ _id: req.query.groupId }).exec();
      if (id) {
        res.status(403);
        return res.json(errorFunction(true, "group deleted sucessfully"))
      }
      else {
        res.status(500);
        return res.json(errorFunction(true, "error while deleting group"))
      }
    } else {
      return res.status(200).json(errorFunction(false, "Unknow Group", groupDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.getGroupDetails = async (req, res, next) => {
  try {
    const groupDetail = await Group.findOne({ _id: req.query.groupId }).exec();
    if (!groupDetail) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown group"))
    } else {
      return res.status(200).json(errorFunction(false, "group details", groupDetail));
    }
  } catch (error) {
    next(error)
  }
};


exports.subAdminList = async (req, res, next) => {
  try {
    const adminList = await Admin.find();
    if (adminList.length > 0) {
      return res.status(200).json(errorFunction(false, "SubAdmin list", adminList))
    } else {
      return res.json(errorFunction(true, "SubAdmin list not found"));
    }
  } catch (error) {
    next(error);
  }
};

exports.deleteSubAdmin = async (req, res, next) => {
  try {
    const subAdminDetail = await Admin.findOne({ _id: req.query.subAdminId }).exec();
    if (subAdminDetail) {
      const id = await Admin.deleteOne({ _id: req.query.subAdminId }).exec();
      if (id) {
        res.status(403);
        return res.json(errorFunction(true, "SubAdmin deleted sucessfully"))
      }
      else {
        res.status(500);
        return res.json(errorFunction(true, "error while deleting SubAdmin"))
      }
    } else {
      return res.status(200).json(errorFunction(false, "Unknow SubAdmin", subAdminDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.subAdminDetails = async (req, res, next) => {
  try {
    const subAdminDetails = await Admin.findOne({ _id: req.query.subAdminId }).exec();
    if (!subAdminDetails) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown Sub Admin"))
    } else {
      return res.status(200).json(errorFunction(false, "Sub Admin details", subAdminDetails));
    }
  } catch (error) {
    next(error)
  }
};

exports.updateSubAdmin = async (req, res, next) => {
  const subAdminDetail = await Admin.findOne({ _id: req.body.subAdminId }).exec();
  if (subAdminDetail) {
    let post = await Admin.updateOne({ _id: req.body.subAdminId }, {
      userName: req.body.userName,
      email: req.body.email, 
      roles: req.body.roles,
      access: req.body.access,     
      password: req.body.password,
      mobileNumber: req.body.mobileNumber,
      updatedAt: Date.now,
    })
    return res.status(200).json(errorFunction(false, "Group details updated successfully"))
  }
  else {
    return res.status(500).json(errorFunction(true, "Unknown group not found"));
  }
};
/*---------------------------- Gafiq Work End --------------------------------------------*/



