const errorFunction = require('../utils/errorFunction');

const Leverage = require('../model/leverage');

const Spread = require('../model/Spread');

const User_profit = require('../model/userprofit');

const Convertcurreny = require('../model/convertcurreny');

// **************** Real Time Forex and CFD Data With NodeJS Websocket *****************************//

// https://tradermade.com/tutorials/real-time-forex-and-cfd-data-with-nodejs-websocket/

exports.leverageExchanges = async (req, res, next) => {

    try {

        const userid = req.params.userid

        const { symbol, leveragetype, avail_balance, leverage, status } = req.body

        if (!(avail_balance && leveragetype && symbol && leverage)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        //-------------------------------  required_margin -------------------------------//

        let required_margin = (Number(avail_balance.replace(/\,/g, '')) / Number(leverage)).toFixed()

        //------------------------------- Margin Balance ---------------------------------//

        // ------------------------------- Free Margin -------------------------------//

        let Margin_Balance = (Number(avail_balance.replace(/\,/g, '')) * Number(leverage)).toFixed(2)

        const checking = await Leverage.findOne({ userid: userid });

        if (!checking) {

            Leverage.create({
                userid: userid,
                avail_balance: avail_balance,
                leveragetype: leveragetype,
                symbol: symbol,
                leverage: leverage,
                Margin_Balance: Margin_Balance,
                required_margin: required_margin,
                status: status
            })

            return res.status(200).json({
                "is_error": false,
                symbol,
                leveragetype,
                required_margin,
                Margin_Balance,
                userid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "User Id Already Add"));
        }

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

};


exports.editleverage = async (req, res, next) => {

    let leverage = await Leverage.findById(req.params.id);

    if (!leverage) {
        return res.json({
            status: false,
            message: "User Leverage Not Found",
        });
    }
    try {
        const updatedleverage = await Leverage.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updatedleverage,
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}



exports.Spread = async (req, res, next) => {

    try {

        const userid = req.params.userid

        const adminid = req.params.adminid

        const { bidvalue, askvalue, xvalue, digits, status } = req.body

        if (!(bidvalue && askvalue && xvalue)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        // --------------------ASK price Calculation  -----------------//

        let Askplatform = (Number(bidvalue.replace(/\,/g, '')) + (Number(askvalue.replace(/\,/g, '')) - Number(bidvalue.replace(/\,/g, ''))) * Number(xvalue.replace(/\,/g, '')) / 2).toFixed(Number(digits))

        // --------------------BID price Calculation  -----------------//

        let bidplatform = (Number(bidvalue.replace(/\,/g, '')) - (Number(askvalue.replace(/\,/g, '')) - Number(bidvalue.replace(/\,/g, ''))) * Number(xvalue.replace(/\,/g, '')) / 2).toFixed(Number(digits))

        let Spread_value = (Askplatform - bidplatform).toFixed(Number(digits))

        const checking = await Spread.findOne({ userid: userid });

        if (!checking) {

            Spread.create({
                userid: userid,
                adminid: adminid,
                bidvalue: bidvalue,
                askvalue: askvalue,
                xvalue: xvalue,
                Askplatform: Askplatform,
                bidplatform: bidplatform,
                Spread: Spread_value,
                status: status
            })

            return res.status(200).json({
                "is_error": false,
                Askplatform,
                bidplatform,
                Spread_value,
                userid,
                adminid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "User Id Already Add"));
        }

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

};


exports.editSpread = async (req, res, next) => {

    let spread = await Spread.findById(req.params.id);

    if (!spread) {
        return res.json({
            status: false,
            message: "User Leverage Not Found",
        });
    }
    try {
        const updatedspread = await Spread.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updatedspread,
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}

exports.user_profit = async (req, res, next) => {

    // 1.0 Lot = 100000(units) 

    // Contract size = Lots * Contract size per Lot //

    // Pipvalue (Forex) = (1 Pip / Exchange rate of quote currency to USD) * Lot * Contract size per Lot //         

    // Leverage-adjusted margin = Trade volume × Contract size × Price × Margin percentage / 100

    try {

        const userid = req.params.userid

        const adminid = req.params.adminid

        const { Contract_size, lot_size, price, leverage, pip, CurrentPrice, OpenPrice, status } = req.body

        if (!(Contract_size && lot_size && price && pip && leverage && CurrentPrice && OpenPrice)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        // --------------------- Exchange rate of quote currency to USD --------------------------------- //

        let Account_currency = (Number(Contract_size.replace(/\,/g, '')) * Number(lot_size) * Number(price) / Number(leverage)).toFixed(Number(2))

        //------------------------------------- Pipvalue (Forex) -------------------------------------------//

        let pipvalue = (Number(pip) / Number(CurrentPrice) * Number(lot_size) * Number(Contract_size)).toFixed(Number(2))

        // --------------------Profit Calculation for BUY  -----------------//

        let BUY_Profit_Calculation = ((Number(CurrentPrice) - Number(OpenPrice)) * Number(Contract_size) * Number(lot_size)).toFixed(Number(2))

        // -------------------- Profit Calculation for SELL -----------------//

        let SELL_Profit_Calculation = ((Number(OpenPrice) - Number(CurrentPrice)) * Number(Contract_size) * Number(lot_size)).toFixed(Number(2))

        const checking = await User_profit.findOne({ userid: userid });

        if (!checking) {

            User_profit.create({
                userid: userid,
                adminid: adminid,
                contract_size: Contract_size,
                lot_size: lot_size,
                price: price,
                leverage: leverage,
                pips: pip,
                currentPrice: CurrentPrice,
                openPrice: OpenPrice,
                Account_currency: Account_currency,
                pipvalue: pipvalue,
                buy_profit_calculation: BUY_Profit_Calculation,
                sell_profit_calculation: SELL_Profit_Calculation,
                status: status
            })

            return res.status(200).json({
                "is_error": false,
                Account_currency,
                pipvalue,
                BUY_Profit_Calculation,
                SELL_Profit_Calculation,
                userid,
                adminid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "User Id Already Add"));
        }

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

};

exports.editUserprofit = async (req, res, next) => {

    let userprofit = await User_profit.findById(req.params.id);

    if (!userprofit) {
        return res.json({
            status: false,
            message: "User Leverage Not Found",
        });
    }
    try {
        const updated_userprofit = await User_profit.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updated_userprofit,
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}

// -------------------------- Convert Curreny ----------------------------------//

exports.Convertcurreny = async (req, res, next) => {

    try {

        const userid = req.params.userid

        const adminid = req.params.adminid

        const { Contract_size, lot_size, CurrentPrice, OpenPrice, symbolprice, status } = req.body

        if (!(Contract_size && lot_size && CurrentPrice && OpenPrice)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        let Convert_curreny = ((Number(CurrentPrice.replace(/\,/g, '')) - Number(OpenPrice.replace(/\,/g, ''))) * Number(Contract_size.replace(/\,/g, '')) * Number(lot_size.replace(/\,/g, ''))).toFixed(Number(2))

        let Eur_curreny = (Number(Convert_curreny) / (Number(CurrentPrice.replace(/\,/g, '')))).toFixed(Number(4))

        // let EUR = String(Eur_curreny) + 'EUR'

        let Usd_curreny = (Number(Eur_curreny) * (Number(symbolprice.replace(/\,/g, '')))).toFixed(Number(2))

        // let USD = String(Usd_curreny) + 'USD'

        const checking = await Convertcurreny.findOne({ userid: userid });

        if (!checking) {

            Convertcurreny.create({
                userid: userid,
                adminid: adminid,
                contract_size: Contract_size,
                lot_size: lot_size,
                currentPrice: CurrentPrice,
                openPrice: OpenPrice,
                symbolprice: symbolprice,
                convertcurreny: Convert_curreny,
                EUR: Eur_curreny + 'EUR',
                USD: Usd_curreny + 'USD',
                status: status
            })


            return res.status(200).json({
                "is_error": false,
                Convert_curreny,
                Eur_curreny,
                Usd_curreny,
                userid,
                adminid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "User Id Already Add"));
        }

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

};

exports.editCurreny = async (req, res, next) => {

    let convert_curreny = await Convertcurreny.findById(req.params.id);

    if (!convert_curreny) {
        return res.json({
            status: false,
            message: "User Leverage Not Found",
        });
    }
    try {
        const updated_convertcurreny = await Convertcurreny.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updated_convertcurreny
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}

exports.Swap1 = async (req, res, next) => {

    try {

        const adminid = req.params.adminid

        const { Pip_Value, Swap_rate, Number_of_Nights, timeof_swaps } = req.body

        if (!(Pip_Value && Swap_rate && Number_of_Nights)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        // Trading 1 lot of EUR/USD (short) with an account denominated in EUR

        let first_Swap = (Number(Pip_Value) * Number(Swap_rate) * Number(Number_of_Nights)) / 10

        let swaps_time = (first_Swap * Number(timeof_swaps)).toFixed(Number(2))

        return res.status(200).json({
            "is_error": false,
            first_Swap,
            swaps_time
        });

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

}

exports.Swap2 = async (req, res, next) => {

    try {

        const { lot_size, Swap_rate, Number_of_Nights, timeof_swaps } = req.body

        if (!(Swap_rate && lot_size && Number_of_Nights)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        //Trading 1 lot (1,000 barrels) of Brent with an account denominated in USD

        let second_Swap = (Number(lot_size.replace(/\,/g, '')) * Number(Swap_rate.replace(/\,/g, '')) * Number(Number_of_Nights.replace(/\,/g, '')))

        let swaps_time = (second_Swap * Number(timeof_swaps)).toFixed(Number(2))

        return res.status(200).json({
            "is_error": false,
            second_Swap,
            swaps_time
        });

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

}
