const errorFunction = require('../utils/errorFunction');

const Symbolgroup = require('../model/symbolgroup');

const Companygroup = require('../model/companygroup');

const Swapgroup = require('../model/swapgroup');

//-------------------------------------------------- Group Symbol -----------------------------------------------------//

exports.Groupsymbol = async (req, res, next) => {

    try {

        const adminid = req.params.id

        const { both_quotes, trade_time, group_symbol, status } = req.body

        if (!(both_quotes && trade_time && group_symbol)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        let close_time = '23:50 to 03:50'

        const checking = await Symbolgroup.findOne({ group_symbol: group_symbol });

        if (!checking) {

            Symbolgroup.create({
                both_quotes: both_quotes,
                trade_time: trade_time,
                close_time: close_time,
                group_symbol: group_symbol,
                status: status,
                adminid: adminid
            })

            return res.status(200).json({
                "is_error": false,
                both_quotes,
                trade_time,
                close_time,
                group_symbol,
                adminid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "Group Already Add"));
        }

    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

}

exports.singlegroupsymbol = async (req, res, next) => {

    const Details = await Symbolgroup.findById(req.params.id);

    if (!Details) {
        return res.json({
            status: false,
            message: "Details no found..",
        });
    }

    res.status(200).json({
        status: true,
        Details,
    });

}

exports.deletegroupsymbol = async (req, res, next) => {

    let id = req.params.id;

    const Record = await Symbolgroup.findByIdAndDelete({ _id: id });

    if (!Record) {
        return res.json({
            status: false,
            message: "Group Symbol no found..",
        });
    }

    res.status(200).json({
        status: true,
        msg: "Group Symbol Deleted Successfully...",
    });

}

exports.editGroupsymbol = async (req, res, next) => {

    let symbolgroup = await Symbolgroup.findById(req.params.id);

    if (!symbolgroup) {
        return res.json({
            status: false,
            message: "Group Symbol Not Found",
        });
    }
    try {
        const updatedsymbolgroup = await Symbolgroup.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updatedsymbolgroup,
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}

exports.viewgroupsymbol = async (req, res, next) => {

    const viewRecords = await Symbolgroup.find();

    let count = viewRecords.length;

    if (!viewRecords) {
        return res.json({
            status: false,
            message: "Group Symbol no found..",
        });
    }

    res.status(200).json({
        status: true,
        TotalViewRecord: count,
        message: "All Records Group Symbol",
        viewRecords,
    });

}

//-------------------------------------------------- Company Group -----------------------------------------------------//


exports.CompanyGroup = async (req, res, next) => {

    try {

        const adminid = req.params.id

        const { Symbol_Group, Swap_Group, Company_name, status } = req.body

        if (!(Symbol_Group && Swap_Group && Company_name)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        const checking = await Companygroup.findOne({ Company_name: Company_name });

        if (!checking) {

            Companygroup.create({
                Symbol_Group: Symbol_Group,
                Swap_Group: Swap_Group,
                Company_name: Company_name,
                status: status,
                adminid: adminid
            })

            return res.status(200).json({
                "is_error": false,
                Symbol_Group,
                Swap_Group,
                Company_name,
                adminid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "Company Already Add"));
        }
    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }
}

exports.editCompanygroup = async (req, res, next) => {

    let companygroup = await Companygroup.findById(req.params.id);

    if (!companygroup) {
        return res.json({
            status: false,
            message: "Company Group Not Found",
        });
    }
    try {
        const updatedcompanygroup = await Companygroup.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updatedcompanygroup,
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}

exports.singlecompanygroup = async (req, res, next) => {

    const Details = await Companygroup.findById(req.params.id);

    if (!Details) {
        return res.json({
            status: false,
            message: "Details no found..",
        });
    }

    res.status(200).json({
        status: true,
        Details,
    });

}

exports.deletecompanygroup = async (req, res, next) => {

    let id = req.params.id;

    const Record = await Companygroup.findByIdAndDelete({ _id: id });

    if (!Record) {
        return res.json({
            status: false,
            message: "Company Group no found..",
        });
    }
    res.status(200).json({
        status: true,
        msg: "Company Group Deleted Successfully...",
    });

}

exports.viewcompanygroup = async (req, res, next) => {

    const viewRecords = await Companygroup.find();

    let count = viewRecords.length;
    if (!viewRecords) {
        return res.json({
            status: false,
            message: "Company Group no found..",
        });
    }

    res.status(200).json({
        status: true,
        TotalViewRecord: count,
        message: "All Records Company Group",
        viewRecords,
    });

}

//-------------------------------------------------- Swap Group -----------------------------------------------------//

exports.Swapgroup = async (req, res, next) => {

    try {

        const userid = req.params.userid

        const adminid = req.params.adminid

        const { group_symbol, symbol_name, Swap1, Swap2, Swap3, status } = req.body

        if (!(group_symbol && symbol_name && Swap1 && Swap2 && Swap3 && adminid && userid)) {
            res.status(400);
            return res.json(errorFunction(true, "Error Adding user"));
        }

        const checking = await Swapgroup.findOne({ symbol_name: symbol_name });

        if (!checking) {

            Swapgroup.create({
                group_symbol: group_symbol,
                symbol_name: symbol_name,
                Swap1: Swap1,
                Swap2: Swap2,
                Swap3: Swap3,
                status: status,
                userid: userid,
                adminid: adminid
            })

            return res.status(200).json({
                "is_error": false,
                group_symbol,
                symbol_name,
                Swap1,
                Swap2,
                Swap3,
                adminid,
                userid
            });

        } else {
            res.status(400);
            return res.json(errorFunction(true, "Company Name Already Add"));
        }
    } catch (error) {
        res.status(400);
        console.log(error);
        return res.json(errorFunction(true, "Error Adding user"));
    }

}

exports.editSwapgroup = async (req, res, next) => {

    let swapgroup = await Swapgroup.findById(req.params.id);

    if (!swapgroup) {
        return res.json({
            status: false,
            message: "Swap Group Not Found",
        });
    }
    try {
        const updatedswapgroup = await Swapgroup.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: "failed",
        });

        res.status(200).json({
            status: true,
            updatedswapgroup,
        });
    } catch (error) {
        res.status(404).json({
            status: "failed",
            error,
        });
    }

}

exports.singleswapgroup = async (req, res, next) => {

    const Details = await Swapgroup.findById(req.params.id);

    if (!Details) {
        return res.json({
            status: false,
            message: "Details no found..",
        });
    }
    res.status(200).json({
        status: true,
        Details,
    });

}

exports.deleteswapgroup = async (req, res, next) => {

    let id = req.params.id;

    const Record = await Swapgroup.findByIdAndDelete({ _id: id });
    if (!Record) {
        return res.json({
            status: false,
            message: "Swap Group no found..",
        });
    }
    res.status(200).json({
        status: true,
        msg: "Swap Group Deleted Successfully...",
    });

}

exports.viewswapgroup = async (req, res, next) => {

    const viewRecords = await Swapgroup.find();

    let count = viewRecords.length;
    if (!viewRecords) {
        return res.json({
            status: false,
            message: "Swap Group no found..",
        });
    }

    res.status(200).json({
        status: true,
        TotalViewRecord: count,
        message: "All Records Swap Group",
        viewRecords,
    });

}





