const User = require('../model/user');
const Support = require('../model/support');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const AWS = require("aws-sdk");
const moment = require('moment');
const path = require('path');

const errorFunction = require('../utils/errorFunction');
const securePassword = require('../utils/securePassword')
const services = require('../services/commonServices')
const sendEmailServices = require('../utils/ses')
const EmailService = require('../services/emailService')
const Leverage = require('../model/leverage');
const Swapgroup = require('../model/swapgroup');

exports.signupUser = async (req, res, next) => {
  try {
    const existingUser = await User.findOne({ email: req.body.email, }).lean(true);
    if (existingUser) {
      res.status(403);
      return res.json(errorFunction(true, "User Already Exists"));
    } else {
      const hashedPassword = await securePassword(req.body.password);
      const newUser = await User.create({
        title: req.body.title,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPassword,
        mobileNumber: req.body.mobileNumber,
        country: req.body.country,
        state: req.body.state,
        address: req.body.address,
        postCode: req.body.postCode,
        is_active: false,
        status: true,
        createdAt: moment().format("ll"),
        updatedAt: new Date().getTime()
      })
      if (newUser) {
        let id = { userId: newUser._id }
        res.status(201);
        return res.json(errorFunction(false, "User Created", id));
      } else {
        res.status(403);
        return res.json(errorFunction(true, "Error Creating User"));
      }
    }
  } catch (error) {
    res.status(400);
    console.log(error);
    return res.json(errorFunction(true, "Error Adding user"));
  }
};

exports.loginUser = async (req, res, next) => {
  const { email, password } = req.body
  const user = await User.findOne({ email })
  if (user && (await bcrypt.compare(password, user.password))) {
    let payload = {
      "userId": user._id,
      "email": user.email
    }
    const token = await jwt.sign({ id: user._id }, process.env.JWT_SECRET_KEY, {
      expiresIn: process.env.JWT_TOKEN_EXPIRED
    })
    let updateToken = await User.updateOne({ _id: user._id }, { $set: { authToken: token, is_active: true } });
    res.status(200);
    return res.json(errorFunction(false, "Login Successfully", user));
  } else {
    res.status(403);
    return res.json(errorFunction(true, "Invalid credentials"));
  }
}

exports.forgotPassword = async (req, res, next) => {
  const { email } = req.body
  try {
    let user = await services.userDetails(email);
    if (!user) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown user"));
    }
    const newPassword = await services.generatePassword();
    const hashedPassword = await securePassword(newPassword);
    const sendEmail = await sendEmailServices(email, newPassword);
    const updatePass = await services.updatePassword(email, hashedPassword)
    return res.status(200).json(errorFunction(false, "Thank you! An email has been sent to " + email + " email id. Please check your inbox."))
  } catch (error) {
    console.log(error)
    next();
  }
};

exports.getUserDetailsById = async (req, res, next) => {
  const userid = req.query.userId;
  try {
    const userDetail = await User.findOne({ _id: userid }).exec();
    if (!userDetail) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown user"))
    } else {
      const swapGroups = await Swapgroup.find({ userid }, { _id: 1, group_symbol: 1, symbol_name: 1, Swap1: 1, Swap2: 1, Swap3: 1, adminid: 1, status: 1 });
      const leverages = await Leverage.find({ userid }, { _id: 1, symbol: 1, leveragetype: 1, avail_balance: 1, leverage: 1, Margin_Balance: 1, required_margin: 1, status: 1 });
      return res.status(200).json(errorFunction(false, "user details", {user: userDetail, swapGroups, leverages}));
    }
  } catch (error) {
    next(error)
  }
};

exports.getUserList = async (req, res, next) => {
  const userList = await User.find().exec();
  if (userList.length > 0) {
    return res.status(200).json(errorFunction(false, "user list", userList))
  } else {
    return res.json(errorFunction(true, "user list not found"));
  }
};

exports.logout = async (req, res, next) => {
  const user = await User.findById({ _id: req.body.userId });
  if (!user) {
    return res.json(errorFunction(true, "logout successfully"));
  } else {

  }
};


exports.activeUser = async (req, res, next) => {
  try {
    const activeUser = await User.find({ is_active: true }).sort({ "updatedAt": -1 });
    if (activeUser.length > 0) {
      return res.status(200).json(errorFunction(false, "Active User List", activeUser))
    } else {
      return res.json(errorFunction(true, "all users are offline"));
    }
  } catch (error) {
    next(error)
  }
};

exports.inactiveUser = async (req, res, next) => {
  try {
    const inactiveUser = await User.find({ is_active: false }).sort({ "updatedAt": -1 });
    if (inactiveUser.length > 0) {
      return res.status(200).json(errorFunction(false, "Inactive User List", inactiveUser))
    } else {
      return res.json(errorFunction(true, "no inactive users"));
    }
  } catch (error) {
    next(error)
  }
};

exports.updateProfile = async (req, res, next) => {
  const userDetail = await User.findOne({ _id: req.body.userId }).exec();
  if (userDetail) {
    let post = await User.updateOne({ _id: req.body.userId }, {
      $set: req.body
    })
    return res.status(200).json(errorFunction(false, "Profile details updated successfully"))
  } else {
    return res.json(errorFunction(true, "user not authenticated"));
  }
};

exports.changePassword = async (req, res, next) => {
  const userList = await User.find({ _id: req.body.userId });
  return res.status(200).json(errorFunction(false, "change password api in development"))
};


exports.contactUs = async (req, res, next) => {
  try {
    const newQuery = await Support.create({
      userId: req.body.userId,
      subject: req.body.subject,
      content: req.body.content,
    })
    if (newQuery) {
      res.status(201);
      return res.json(errorFunction(false, "Query Created"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "Error of create query"));
    }
  } catch (error) {
    res.status(400);
    console.log(error);
    return res.json(errorFunction(true, "Error Adding user"));
  }
};

exports.selectPlanDetails = async (req, res, next) => {
  const { userId, planType } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const PlanDetails = await services.selectPlanDetails(userId, planType)
      if (PlanDetails === true) {
        return res.status(200).json(errorFunction(false, "Plan Added Successfully"));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};

exports.accountCurrency = async (req, res, next) => {
  const { userId, currency } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const PlanDetails = await services.addAccountCurrency(userId, currency)
      if (PlanDetails === true) {
        return res.status(200).json(errorFunction(false, "Currency Added Successfully"));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};
exports.riskMode = async (req, res, next) => {
  const { userId, mode } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const PlanDetails = await services.addriskMode(userId, mode)
      if (PlanDetails === true) {
        return res.status(200).json(errorFunction(false, "Risk Mode Added Successfully"));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};

exports.downloadSetup = async (req, res, next) => {
  let filePath = path.join(__dirname, '../public/heroku-x64.exe');
  res.download(filePath);
}


/*---------------------------- Gafiq Work Start --------------------------------------------*/

exports.addAccountBalance = async (req, res, next) => {
  const { userId, amount } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      var newAmount = user.accountBalance + parseFloat(req.body.amount);
      console.log('test');
      const planDetails = await services.addToBalance(userId, newAmount, "Cr", user.accountBalance, parseFloat(req.body.amount))
      console.log('plan details', planDetails);
      var userUpdatedDetails = await User.findOne({ _id: userId });
      if (planDetails === true) {
        return res.status(200).json(errorFunction(false, "Amount Added Successfully", userUpdatedDetails));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};


/*
exports.addAccountBalance = async (req, res, next) => {
  const { userId, amount } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      var newAmount = user.accountBalance + parseFloat(req.body.amount);
      const planDetails = await services.addToBalance(userId, newAmount)
      var userUpdatedDetails = await User.findOne({ _id: userId });
      if (planDetails === true) {
        return res.status(200).json(errorFunction(false, "Amount Added Successfully", userUpdatedDetails));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};
*/

/*
  exports.addAccountBalance = async (req, res, next) => {
    const { userId, amount } = req.body
    try {
      const user = await User.findOne({ _id: userId });
      if (user) {
        const credit = await services.addToBalance(userId, amount)
        if (credit === true) {
          return res.status(200).json(errorFunction(false, "Amount Added Successfully", user.accountBalance));
        }
        return res.status(403).json(errorFunction(true, "something went wrong"));
      } else {
        res.status(403);
        return res.json(errorFunction(true, "user not authenticated"))
      }
    } catch (error) {
      next()
    }
  };
 */



exports.searchUser = async (req, res, next) => {
  if ((parseInt(req.query.searchQuery) || parseFloat(req.query.searchQuery)) && req.query.searchQuery.length == 10) {
    const user = await User.find({ mobileNumber: req.query.searchQuery });
    if (user) {
      return res.status(200).json(errorFunction(false, "User Search List", user))
    }
    else {
      return res.status(500).json(errorFunction(true, "error"));
    }
  }
  else if ((parseInt(req.query.searchQuery) || parseFloat(req.query.searchQuery)) && req.query.searchQuery.length != 10) {
    const user = await User.find({ accountBalance: req.query.searchQuery });
    if (user) {
      return res.status(200).json(errorFunction(false, "User Search List", user))
    }
    else {
      return res.status(500).json(errorFunction(true, "error"));
    }
  }
  else {
    var query =
    {
      $or:
        [
          { firstName: { $regex: req.query.searchQuery, $options: 'i' } }
          , { lastName: { $regex: req.query.searchQuery, $options: 'i' } }
          , { country: { $regex: req.query.searchQuery, $options: 'i' } }
          , { email: { $regex: req.query.searchQuery, $options: 'i' } }
          , { mobileNumber: { $regex: req.query.searchQuery, $options: 'i' } }
        ]
    }
    User.find(query, function (err, data) {
      if (!err) {
        return res.status(200).json(errorFunction(false, "User Search List", data))
      }
      else {
        return res.status(500).json(errorFunction(true, "error"));
      }
    });
  }
};


exports.getUserBalance = async (req, res, next) => {
  try {
    const userDetail = await User.findOne({ _id: req.query.userId }).select({ "email": 1, "_id": 0, "accountBalance": 2, "userId": req.query.userId });
    if (!userDetail) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown user"))
    } else {
      return res.status(200).json(errorFunction(false, "user details", userDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.deductAccountBalance = async (req, res, next) => {
  const { userId, amount } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      if (user.accountBalance < parseFloat(req.body.amount)) {
        res.status(403);
        return res.json(errorFunction(true, "your account balance is insufficient"))
      }
      else {
        var newAmount = user.accountBalance - parseFloat(req.body.amount);
        const planDetails = await services.deductAccountBalance(userId, newAmount, "Dr", user.accountBalance, parseFloat(req.body.amount))
        var userUpdatedDetails = await User.findOne({ _id: userId });
        if (planDetails === true) {
          return res.status(200).json(errorFunction(false, "Amount Deducted Successfully", userUpdatedDetails));
        }
        return res.status(403).json(errorFunction(true, "something went wrong"));
      }
    }
    else {
      res.status(403);
      return res.json(errorFunction(true, "user not found"))
    }
  } catch (error) {
    next()
  }
};
/*
  exports.deductAccountBalance = async (req, res, next) => {
    const { userId, amount } = req.body
    try {
      const user = await User.findOne({ _id: userId });
      if (user) {
        if (user.accountBalance < parseFloat(req.body.amount)) {
          res.status(403);
          return res.json(errorFunction(true, "Low Account Balance"))
        }
        else {
          var newAmount = user.accountBalance - parseFloat(req.body.amount);
          const planDetails = await services.deductAccountBalance(userId, newAmount)
          var userUpdatedDetails = await User.findOne({ _id: userId });
          if (planDetails === true) {
            return res.status(200).json(errorFunction(false, "Amount Deducted Successfully", userUpdatedDetails));
          }
          return res.status(403).json(errorFunction(true, "something went wrong"));
        }
      }
      else {
        res.status(403);
        return res.json(errorFunction(true, "user not authenticated"))
      }
    } catch (error) {
      next()
    }
  };
*/

exports.generateNotification = async (req, res, next) => {
  const { userId, transactionType, transactionAmount, type } = req.body
  try {
    const createdAt = new Date().getTime()
    const updatedAt = new Date().getTime()
    const processed = 0;
    const newNotification = new Notification({ userId, transactionType, transactionAmount, type, processed, createdAt, updatedAt });
    await newNotification.save().then((response) => {
      EmailService.sendNotificationAdminEmail(userId, newNotification.id);
      res.status(201);
      return res.json(errorFunction(false, "Notification Created", response));
    })
  } catch (error) {
    res.status(403);
    return res.json(errorFunction(true, "Something went wrong!"));
  }
};


exports.approveNotification = async (req, res, next) => {
  try {
    const notificationList = await Notification.findOne({ _id: req.query.notificationId });
    if (notificationList) {

      const updatedNotification = await Notification.updateOne({ _id: req.query.notificationId }, {
        processed: 1,
        updatedAt: Date.now,
      });
      var userDetails = await User.findOne({ _id: req.body.userId });
      if (notificationList.transactionType === "Deposit") {
        const result = await services.addToBalance(notificationList.userId, parseFloat(notificationList.transactionAmount) + parseFloat(userDetails.accountBalance)
          , "Cr", notificationList.transactionAmount + userDetails.accountBalance, parseFloat(notificationList.transactionAmount));
      }
      else {
        const result = await services.deductAccountBalance(notificationList.userId, parseFloat(userDetails.accountBalance) - parseFloat(notificationList.transactionAmount)
          , "Dr", notificationList.transactionAmount + userDetails.accountBalance, parseFloat(notificationList.transactionAmount));
      }
      EmailService.sendNotificationUserEmail(notificationList.userId, notificationList.notificationId);
      res.status(200);
      return res.json(errorFunction(false, "Notification Processed", notificationList));
    }
    else {
      res.status(403);
      return res.json(errorFunction(true, "Invalid Notification Details!"));
    }
  } catch (error) {
    console.log(error);
    res.status(403);
    return res.json(errorFunction(true, "Something went wrong!"));
  }
};


exports.getNotificationList = async (req, res, next) => {
  const notificationList = await Notification.find({ processed: 0 });
  if (notificationList.length > 0) {
    return res.status(200).json(errorFunction(false, "Notifications list", notificationList))
  } else {
    return res.json(errorFunction(true, "Notifications list not found"));
  }
};

exports.getPendingNotificationListByUser = async (req, res, next) => {
  var query =
  {
    $and:
      [
        { userId: req.query.userId }
        , { processed: 0 }
      ]
  }

  const notificationList = await Notification.find(query);
  if (notificationList.length > 0) {
    return res.status(200).json(errorFunction(false, "Notifications list", notificationList))
  } else {
    return res.json(errorFunction(true, "Notifications list not found"));
  }
};

exports.getProcessedNotificationListByUser = async (req, res, next) => {
  var query =
  {
    $and:
      [
        { userId: req.query.userId }
        , { processed: 1 }
      ]
  }

  const notificationList = await Notification.find(query);
  if (notificationList.length > 0) {
    return res.status(200).json(errorFunction(false, "Notifications list", notificationList))
  } else {
    return res.json(errorFunction(true, "Notifications list not found"));
  }
};

exports.getNotificationListByUser = async (req, res, next) => {
  const notificationList = await Notification.find({ _id: req.query.userId });
  if (notificationList.length > 0) {
    return res.status(200).json(errorFunction(false, "Notifications list", notificationList))
  } else {
    return res.json(errorFunction(true, "Notifications list not found"));
  }
};




/*--------------------------------- Gafiq Work End --------------------------------------------*/

