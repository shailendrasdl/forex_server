const errorFunction = require("../utils/errorFunction");

const Order = require("../model/order");
const User = require("../model/user");

exports.buyLimit = async (req, res, next) => {
  const id = req.body.userid;
  const user = await User.findById(id);

  if (req.body.at_price >= req.body.currentPrice) {
    return res.status(404).json({
      success: false,
      meg: "Open Price Should be less than current ASK price..",
    });
  } else {
    if (
      req.body.take_profit < req.body.currentPrice ||
      req.body.take_profit === req.body.currentPrice
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be more than current BID price..",
      });
    }
    if (
      req.body.take_profit < req.body.at_price ||
      req.body.take_profit === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be more than Open price..",
      });
    }
    if (
      req.body.stop_loss > req.body.currentPrice ||
      req.body.stop_loss === req.body.currentPrice
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be less than current BID price..",
      });
    }
    if (
      req.body.stop_loss > req.body.at_price ||
      req.body.stop_loss === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be less than Open price..",
      });
    }

    const adminid = req.body.adminid;

    const userid = req.body.userid;

    const {
      symbol,
      volume,
      stop_loss,
      take_profit,
      comment,
      order_type,
      type,
      at_price,
      expiry,
      status,
      currentPrice,
    } = req.body;

    try {
      const doc = new Order({
        symbol: symbol,
        volume: volume,
        stop_loss: stop_loss,
        take_profit: take_profit,
        comment: comment,
        order_type: order_type,
        panding_order: {
          type: type,
          at_price: at_price,
          expiry: expiry,
        },
        status: status,
        userid: userid,
        adminid: adminid,
        currentPrice: currentPrice,
      });

      // available Amount v/s order value
      var orderValue = volume * currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }
      await doc.save();

      res.status(201).json({
        status: true,
        message: "Buy Limit created Successfully",
        doc,
      });
    } catch (err) {
      console.log(err);
      return res.status(404).json({ message: "rejected..." });
    }
  }
};

exports.modifyBuyLimit = async (req, res, next) => {
  const buyLimit = await Order.findById(req.params.id);
  const userId = buyLimit.userid;

  const user = await User.findById(userId);

  if (!buyLimit) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }
  try {
    if (
      req.body.at_price > buyLimit.currentPrice ||
      req.body.at_price === buyLimit.currentPrice ||
      req.body.at_price > req.body.currentPrice ||
      req.body.at_price === req.body.currentPrice
    ) {
      res.status(404).json({
        success: false,
        meg: "Open Price Should be less than current ASK price..",
      });
    } else {
      if (
        req.body.stop_loss > buyLimit.currentPrice ||
        req.body.stop_loss === buyLimit.currentPrice ||
        req.body.stop_loss > req.body.currentPrice ||
        req.body.stop_loss === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be less than current BID price..",
        });
      }
      if (
        req.body.take_profit < buyLimit.currentPrice ||
        req.body.take_profit === buyLimit.currentPrice ||
        req.body.take_profit < req.body.currentPrice ||
        req.body.take_profit === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be more than current BID price..",
        });
      }
      if (
        req.body.take_profit < req.body.at_price ||
        req.body.take_profit === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be more than Open price..",
        });
      }
      if (
        req.body.stop_loss > req.body.at_price ||
        req.body.stop_loss === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be less than Open price..",
        });
      }

      // available Amount v/s order value
      var orderValue = req.body.volume * buyLimit.currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }

      const data = {
        panding_order: {
          type: buyLimit.panding_order.type,
          at_price: req.body.at_price,
          expiry: buyLimit.panding_order.expiry,
        },
        stop_loss: req.body.stop_loss || buyLimit.stop_loss,
        take_profit: req.body.take_profit || buyLimit.take_profit,
        volume: req.body.volume || buyLimit.volume,
      };

      const OrderDetails = await Order.findByIdAndUpdate(req.params.id, data, {
        new: true,
        runValidators: true,
        useFindAndModify: false,
      });
      res.status(200).json({
        status: true,
        msg: " Price Detail Successfully Update...",
        OrderDetails,
      });
    }
  } catch (error) {
    res.status(400).json({
      status: false,
      msg: error.message,
    });
  }
};

exports.deleteBuyLimit = async (req, res, next) => {
  try {
    const deleteBuyLimit = await Order.findByIdAndDelete(req.params.id);
    res.status(200).send({
      success: true,
      msg: "Order Deleted Successfully...",
    });
  } catch (error) {
    res.status(400).send({
      success: false,
      msg: error.message,
    });
  }
};

// Byu Stop

exports.buyStop = async (req, res, next) => {
  const id = req.body.userid;
  const user = await User.findById(id);

  if (req.body.at_price < req.body.currentPrice) {
    res.status(404).json({
      success: false,
      meg: "Open Price Should be more than current ASK price..",
    });
  } else {
    if (
      req.body.take_profit < req.body.currentPrice ||
      req.body.take_profit === req.body.currentPrice
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be more than current BID price..",
      });
    }
    if (
      req.body.take_profit < req.body.at_price ||
      req.body.take_profit === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be more than Open price..",
      });
    }
    if (
      req.body.stop_loss > req.body.currentPrice ||
      req.body.stop_loss === req.body.currentPrice
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be less than current BID price..",
      });
    }
    if (
      req.body.stop_loss > req.body.at_price ||
      req.body.stop_loss === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be less than Open price..",
      });
    }
    const adminid = req.body.adminid;
    const userid = req.body.userid;
    const {
      symbol,
      volume,
      stop_loss,
      take_profit,
      comment,
      order_type,
      type,
      at_price,
      expiry,
      status,
      currentPrice,
    } = req.body;

    try {
      const doc = new Order({
        symbol: symbol,
        volume: volume,
        stop_loss: stop_loss,
        take_profit: take_profit,
        comment: comment,
        order_type: order_type,
        panding_order: {
          type: type,
          at_price: at_price,
          expiry: expiry,
        },
        status: status,
        userid: userid,
        adminid: adminid,
        currentPrice: currentPrice,
      });

      // available Amount v/s order value
      var orderValue = volume * currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }
      await doc.save();
      res.status(201).json({
        status: true,
        message: "Buy Stop created Successfully",
        doc,
      });
    } catch (err) {
      console.log(err);
      return res.status(404).json({ message: "rejected..." });
    }
  }
};

exports.modifyBuyStop = async (req, res, next) => {
  const buyStop = await Order.findById(req.params.id);

  if (!buyStop) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }
  const userId = buyStop.userid;

  const user = await User.findById(userId);
  try {
    if (req.body.at_price < buyStop.currentPrice) {
      res.status(404).json({
        success: false,
        meg: "Open Price Should be more than current ASK price..",
      });
    } else {
      if (
        req.body.take_profit < buyStop.currentPrice ||
        req.body.take_profit === buyStop.currentPrice ||
        req.body.take_profit < req.body.currentPrice ||
        req.body.take_profit === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be more than current BID price..",
        });
      }
      if (
        req.body.take_profit < req.body.at_price ||
        req.body.take_profit === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be more than Open price..",
        });
      }
      if (
        req.body.stop_loss > buyStop.currentPrice ||
        req.body.stop_loss === buyStop.currentPrice ||
        req.body.stop_loss > req.body.currentPrice ||
        req.body.stop_loss === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be less than current BID price..",
        });
      }
      if (
        req.body.stop_loss > req.body.at_price ||
        req.body.stop_loss === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be less than Open price..",
        });
      }

      // available Amount v/s order value
      var orderValue = req.body.volume * buyStop.currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }
      const data = {
        panding_order: {
          type: buyStop.panding_order.type,
          at_price: req.body.at_price,
          expiry: buyStop.panding_order.expiry,
        },
        stop_loss: req.body.stop_loss || buyStop.stop_loss,
        take_profit: req.body.take_profit || buyStop.take_profit,
        volume: req.body.volume || buyStop.volume,
      };

      const OrderDetails = await Order.findByIdAndUpdate(req.params.id, data, {
        new: true,
        runValidators: true,
        useFindAndModify: false,
      });
      res.status(200).json({
        status: true,
        msg: " Price Detail Successfully Update...",
        OrderDetails,
      });
    }
  } catch (error) {
    res.status(400).json({
      status: false,
      msg: error.message,
    });
  }
};

exports.deleteBuyStop = async (req, res, next) => {
  try {
    const deleteBuyStop = await Order.findByIdAndDelete(req.params.id);

    if (!deleteBuyStop) {
      res.status(400).send({
        success: false,
        msg: "Order Does Not Exists..",
      });
    }
    res.status(200).send({
      success: true,
      msg: "Order Deleted Successfully...",
    });
  } catch (error) {
    res.status(400).send({
      success: false,
      msg: error.message,
    });
  }
};

// Sell Limit

exports.sellLimit = async (req, res, next) => {
  const id = req.body.userid;
  const user = await User.findById(id);

  if (req.body.at_price < req.body.currentPrice) {
    res.status(404).json({
      success: false,
      meg: "Open Price Should be more than current BID price..",
    });
  } else {
    if (
      req.body.take_profit > req.body.currentPrice ||
      req.body.take_profit === req.body.currentPrice
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be less than current ASK price..",
      });
    }
    if (
      req.body.take_profit > req.body.at_price ||
      req.body.take_profit === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be less than Open price..",
      });
    }
    if (
      req.body.stop_loss < req.body.currentPrice ||
      req.body.stop_loss === req.body.currentPrice
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be more than current ASK price..",
      });
    }
    if (
      req.body.stop_loss < req.body.at_price ||
      req.body.stop_loss === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be more than Open price..",
      });
    }
    const adminid = req.body.adminid;

    const userid = req.body.userid;

    const {
      symbol,
      volume,
      stop_loss,
      take_profit,
      comment,
      order_type,
      type,
      at_price,
      expiry,
      status,
      currentPrice,
    } = req.body;

    try {
      const doc = new Order({
        symbol: symbol,
        volume: volume,
        stop_loss: stop_loss,
        take_profit: take_profit,
        comment: comment,
        order_type: order_type,
        panding_order: {
          type: type,
          at_price: at_price,
          expiry: expiry,
        },
        status: status,
        userid: userid,
        adminid: adminid,
        currentPrice: currentPrice,
      });

      // available Amount v/s order value
      var orderValue = volume * currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }

      await doc.save();
      res.status(201).json({
        status: true,
        message: "Sell Limit created Successfully",
        doc,
      });
    } catch (err) {
      console.log(err);
      return res.status(404).json({ message: "rejected..." });
    }
  }
};

exports.modifySellLimit = async (req, res, next) => {
  const sellLimit = await Order.findById(req.params.id);

  if (!sellLimit) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }

  const userId = sellLimit.userid;

  const user = await User.findById(userId);

  try {
    if (req.body.at_price <= sellLimit.currentPrice) {
      res.status(404).json({
        success: false,
        meg: "Open Price Should be more than current BID price..",
      });
    } else {
      if (
        req.body.take_profit > sellLimit.currentPrice ||
        req.body.take_profit === sellLimit.currentPrice ||
        req.body.take_profit > req.body.currentPrice ||
        req.body.take_profit === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be less than current ASK price..",
        });
      }
      if (
        req.body.take_profit > req.body.at_price ||
        req.body.take_profit === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be less than Open price..",
        });
      }
      if (
        req.body.stop_loss < sellLimit.currentPrice ||
        req.body.stop_loss === sellLimit.currentPrice ||
        req.body.stop_loss < req.body.currentPrice ||
        req.body.stop_loss === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be more than current ASK price..",
        });
      }
      if (
        req.body.stop_loss < req.body.at_price ||
        req.body.stop_loss === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be more than Open price..",
        });
      }

      // available Amount v/s order value
      var orderValue = req.body.volume * sellLimit.currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }

      const data = {
        panding_order: {
          type: sellLimit.panding_order.type,
          at_price: req.body.at_price,
          expiry: sellLimit.panding_order.expiry,
        },
        take_profit: req.body.take_profit || sellLimit.take_profit,
        stop_loss: req.body.stop_loss || sellLimit.stop_loss,
        volume: req.body.volume || sellLimit.volume,
      };

      const OrderDetails = await Order.findByIdAndUpdate(req.params.id, data, {
        new: true,
        runValidators: true,
        useFindAndModify: false,
      });
      res.status(200).json({
        status: true,
        msg: " Price Detail Successfully Update...",
        OrderDetails,
      });
    }
  } catch (error) {
    res.status(400).json({
      status: false,
      msg: error.message,
    });
  }
};

exports.deleteSellLimit = async (req, res, next) => {
  const sellLimit = await Order.findById(req.params.id);

  if (!sellLimit) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }
  try {
    const deleteSellLimit = await Order.findByIdAndDelete(req.params.id);
    res.status(200).send({
      success: true,
      msg: "Order Deleted Successfully...",
    });
  } catch (error) {
    return res.status(400).send({
      success: false,
      msg: error.message,
    });
  }
};

// Sell Stop

exports.sellStop = async (req, res, next) => {
  const id = req.body.userid;
  const user = await User.findById(id);

  if (!user) {
    return res.status(404).json({
      success: false,
      meg: "User not Exists...",
    });
  }

  if (req.body.at_price > req.body.currentPrice) {
    return res.status(404).json({
      success: false,
      meg: "Open Price Should be less than current Bid price..",
    });
  } else {
    if (req.body.take_profit > req.body.currentPrice) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be less than current ASK price..",
      });
    }
    if (
      req.body.take_profit > req.body.at_price ||
      req.body.take_profit === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "T/P Should be less than Open price..",
      });
    }
    if (req.body.stop_loss < req.body.currentPrice) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be more than current ASK price..",
      });
    }
    if (
      req.body.stop_loss < req.body.at_price ||
      req.body.stop_loss === req.body.at_price
    ) {
      return res.status(404).json({
        success: false,
        meg: "S/L Should be more than Open price..",
      });
    }
    const adminid = req.body.adminid;
    const userid = req.body.userid;
    const {
      symbol,
      volume,
      stop_loss,
      take_profit,
      comment,
      order_type,
      type,
      at_price,
      expiry,
      status,
      currentPrice,
    } = req.body;

    try {
      const doc = new Order({
        symbol: symbol,
        volume: volume,
        stop_loss: stop_loss,
        take_profit: take_profit,
        comment: comment,
        order_type: order_type,
        panding_order: {
          type: type,
          at_price: at_price,
          expiry: expiry,
        },
        status: status,
        userid: userid,
        adminid: adminid,
        currentPrice: currentPrice,
      });

      // available Amount v/s order value
      var orderValue = volume * currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }

      await doc.save();
      res.status(201).json({
        status: true,
        message: "Sell Stop created Successfully",
        doc,
      });
    } catch (err) {
      console.log(err);
      return res.status(404).json({ message: "rejected..." });
    }
  }
};

exports.modifySellStop = async (req, res, next) => {
  const sellStop = await Order.findById(req.params.id);

  if (!sellStop) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }

  const userId = sellStop.userid;
  const user = await User.findById(userId);

  try {
    if (req.body.at_price > sellStop.currentPrice) {
      res.status(404).json({
        success: false,
        meg: "Open Price Should be less than current ASK price..",
      });
    } else {
      if (
        req.body.take_profit > sellStop.currentPrice ||
        req.body.take_profit === sellStop.currentPrice ||
        req.body.take_profit > req.body.currentPrice ||
        req.body.take_profit === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be less than current ASK price..",
        });
      }
      if (
        req.body.take_profit > req.body.at_price ||
        req.body.take_profit === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "T/P Should be less than Open price..",
        });
      }
      if (
        req.body.stop_loss < sellStop.currentPrice ||
        req.body.stop_loss === sellStop.currentPrice ||
        req.body.stop_loss < req.body.currentPrice ||
        req.body.stop_loss === req.body.currentPrice
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be more than current ASK price..",
        });
      }
      if (
        req.body.stop_loss < req.body.at_price ||
        req.body.stop_loss === req.body.at_price
      ) {
        return res.status(404).json({
          success: false,
          meg: "S/L Should be more than Open price..",
        });
      }

      // available Amount v/s order value
      var orderValue = req.body.volume * sellStop.currentPrice;

      if (orderValue > user.accountBalance) {
        return res.status(404).send({
          success: false,
          msg: "Insufficient Amount...",
        });
      }

      const data = {
        panding_order: {
          type: sellStop.panding_order.type,
          at_price: req.body.at_price,
          expiry: sellStop.panding_order.expiry,
        },
        take_profit: req.body.take_profit || sellStop.take_profit,
        stop_loss: req.body.stop_loss || sellStop.stop_loss,
        volume: req.body.volume || sellStop.volume,
      };

      const OrderDetails = await Order.findByIdAndUpdate(req.params.id, data, {
        new: true,
        runValidators: true,
        useFindAndModify: false,
      });
      res.status(200).json({
        status: true,
        msg: " Price Detail Successfully Update...",
        OrderDetails,
      });
    }
  } catch (error) {
    res.status(400).json({
      status: false,
      msg: error.message,
    });
  }
};
exports.deleteSellStop = async (req, res, next) => {
  const sellStop = await Order.findById(req.params.id);
  if (!sellStop) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }
  try {
    const deleteSellStop = await Order.findByIdAndDelete(req.params.id);
    res.status(200).send({
      success: true,
      msg: "Order Deleted Successfully...",
    });
  } catch (error) {
    res.status(400).send({
      success: false,
      msg: error.message,
    });
  }
};

// Real Order -- OPEN
exports.openOrder = async (req, res, next) => {
  const order = await Order.findById(req.params.id);
  if (!order) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }

  const user = await User.findById(order.userid);
  if (!user) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!user Not Found for this order... ",
    });
  }

  try {
    const data = {
      order_type: "open",
    };

    const OrderDetails = await Order.findByIdAndUpdate(req.params.id, data, {
      new: true,
      runValidators: true,
      useFindAndModify: false,
    });

    // Deduction from Account Balance
    var orderValue = order.volume * order.currentPrice;

    if (orderValue > user.accountBalance) {
      return res.status(404).send({
        success: false,
        msg: "Insufficient Amount...",
      });
    }
    var availableBalance = user.accountBalance - orderValue;

    const amount = {
      accountBalance: availableBalance,
    };
    const userDetails = await User.findByIdAndUpdate(order.userid, amount, {
      new: true,
      runValidators: true,
      useFindAndModify: false,
    });

    res.status(200).send({
      success: true,
      msg: "Order placed Successfully..",
      availableBalance: userDetails.accountBalance,
    });
  } catch (error) {
    res.status(400).send({
      success: false,
      msg: error.message,
    });
  }
};

// Real Order -- Close
exports.closeOrder = async (req, res, next) => {
  const order = await Order.findById(req.params.id);
  if (!order) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!Order Not Found... ",
    });
  }
  const user = await User.findById(order.userid);
  if (!user) {
    return res.status(404).json({
      success: false,
      meg: "OPPS!!user Not Found for this order... ",
    });
  }
  try {
    const data = {
      order_type: "close",
    };

    const orderDetails = await Order.findByIdAndUpdate(req.params.id, data, {
      new: true,
      runValidators: true,
      useFindAndModify: false,
    });

    res.status(200).send({
      success: true,
      orderDetails,
    });
  } catch (error) {
    res.status(400).send({
      success: false,
      msg: error.message,
    });
  }
};

/*---------------------------- currency pair USD not the firstly written one --------------------------------------------*/

exports.buyOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(-3);
    if (result == "USD") {
      const openOrder = (currentPrice - openPrice) * contractSize * lot;
      return res.status(200).json({
        success: true,
        openOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    return res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.buyClosedOrder = async (req, res, next) => {
  const closeprice = req.body.closeprice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(-3);
    if (result == "USD") {
      const closedOrder = (closeprice - openPrice) * contractSize * lot;
      return res.status(200).json({
        success: true,
        closedOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    return res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.sellOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(-3);
    if (result == "USD") {
      const openOrder = (openPrice - currentPrice) * contractSize * lot;

      return res.status(200).json({
        success: true,
        openOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    return res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.sellClosedOrder = async (req, res, next) => {
  const closeprice = req.body.closeprice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(-3);
    if (result == "USD") {
      const closedOrder = (openPrice - closeprice) * contractSize * lot;
      res.status(200).json({
        success: true,
        closedOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

/*---------------------------- currency pair USD is the firstly written one --------------------------------------------*/

exports.firstCurrencyBuyOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(0, 3);
    if (result == "USD") {
      const openOrder =
        ((currentPrice - openPrice) * contractSize * lot) / currentPrice;
      res.status(200).json({
        success: true,
        openOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.firstCurrencyBuyClosedOrder = async (req, res, next) => {
  const closeprice = req.body.closeprice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(0, 3);
    if (result == "USD") {
      const closedOrder =
        ((closeprice - openPrice) * contractSize * lot) / closeprice;
      res.status(200).json({
        success: true,
        closedOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.firstCurrencySellOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(0, 3);
    if (result == "USD") {
      const openOrder =
        ((openPrice - currentPrice) * contractSize * lot) / currentPrice;

      res.status(200).json({
        success: true,
        openOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.firstCurrencySellClosedOrder = async (req, res, next) => {
  const closeprice = req.body.closeprice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;

  try {
    let result = symbol.slice(0, 3);
    if (result == "USD") {
      const closedOrder =
        ((openPrice - closeprice) * contractSize * lot) / closeprice;
      res.status(200).json({
        success: true,
        closedOrder,
      });
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

/*---------------------------- currency pair does not include USD--------------------------------------------*/

exports.withoutCurrencyBuyOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;
  const Price_of_EURUSD = req.body.Price_of_EURUSD || "";
  const Price_of_USDCAD = req.body.Price_of_USDCAD || "";

  try {
    let result = symbol.slice(-3);
    if (result !== "USD") {
      let result = symbol.slice(0, 3);
      if (result !== "USD") {
        const openOrder =
          ((currentPrice - openPrice) * contractSize * lot) / currentPrice;
        if (Price_of_EURUSD) {
          const EURUSD = openOrder * Price_of_EURUSD;
          res.status(200).json({
            success: true,
            EURUSD,
          });
        } else {
          const USDCAD = openOrder / Price_of_USDCAD;
          res.status(200).json({
            success: true,
            USDCAD,
          });
        }
      } else {
        return res.json({
          status: false,
          message: "rejected....",
        });
      }
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.withoutCurrencyBuyClosedOrder = async (req, res, next) => {
  const closeprice = req.body.closeprice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;
  const Price_of_EURUSD = req.body.Price_of_EURUSD || "";
  const Price_of_USDCAD = req.body.Price_of_USDCAD || "";

  try {
    let result = symbol.slice(-3);
    if (result !== "USD") {
      let result = symbol.slice(0, 3);
      if (result !== "USD") {
        const closedOrder =
          ((closeprice - openPrice) * contractSize * lot) / closeprice;
        if (Price_of_EURUSD) {
          const EURUSD = closedOrder * Price_of_EURUSD;
          res.status(200).json({
            success: true,
            EURUSD,
          });
        } else {
          const USDCAD = closedOrder / Price_of_USDCAD;
          res.status(200).json({
            success: true,
            USDCAD,
          });
        }
      } else {
        return res.json({
          status: false,
          message: "rejected....",
        });
      }
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.withoutCurrencySellOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;
  const Price_of_EURUSD = req.body.Price_of_EURUSD || "";
  const Price_of_USDCAD = req.body.Price_of_USDCAD || "";

  try {
    let result = symbol.slice(-3);
    if (result !== "USD") {
      let result = symbol.slice(0, 3);
      if (result !== "USD") {
        const openOrder =
          ((openPrice - currentPrice) * contractSize * lot) / currentPrice;

        if (Price_of_EURUSD) {
          const EURUSD = openOrder * Price_of_EURUSD;
          res.status(200).json({
            success: true,
            EURUSD,
          });
        } else {
          const USDCAD = openOrder / Price_of_USDCAD;
          res.status(200).json({
            success: true,
            USDCAD,
          });
        }
      } else {
        return res.json({
          status: false,
          message: "rejected....",
        });
      }
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

exports.withoutCurrencySellClosedOrder = async (req, res, next) => {
  const closeprice = req.body.closeprice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;
  const Price_of_EURUSD = req.body.Price_of_EURUSD || "";
  const Price_of_USDCAD = req.body.Price_of_USDCAD || "";

  try {
    let result = symbol.slice(-3);
    if (result !== "USD") {
      let result = symbol.slice(0, 3);
      if (result !== "USD") {
        const closedOrder =
          ((openPrice - closeprice) * contractSize * lot) / closeprice;

        if (Price_of_EURUSD) {
          const EURUSD = closedOrder * Price_of_EURUSD;
          res.status(200).json({
            success: true,
            EURUSD,
          });
        } else {
          const USDCAD = closedOrder / Price_of_USDCAD;
          res.status(200).json({
            success: true,
            USDCAD,
          });
        }
      } else {
        return res.json({
          status: false,
          message: "rejected....",
        });
      }
    } else {
      return res.json({
        status: false,
        message: "rejected....",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

/*---------------------------- only for open BUY orders --------------------------------------------*/

exports.daxBuyOpenOrder = async (req, res, next) => {
  const currentPrice = req.body.currentPrice;
  const openPrice = req.body.openPrice;
  const contractSize = req.body.contractSize;
  const lot = req.body.lot;
  const symbol = req.body.symbol;
  const Price_of_EURUSD = req.body.Price_of_EURUSD;

  try {
    if (
      symbol == "DAX" ||
      symbol == "US30" ||
      symbol == "US100" ||
      symbol == "US500" ||
      symbol == "US.Oil" ||
      symbol == "UK.Oil"
    ) {
      const openOrder =
        (currentPrice - openPrice) * contractSize * lot * Price_of_EURUSD;
      res.status(200).json({
        success: true,
        openOrder,
      });
    } else {
      return res.status(400).send({
        success: false,
        msg: "Please Enter valid symbol",
      });
    }
  } catch (error) {
    return res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};

// All Orders by User
exports.orderByUser = async (req, res, next) => {
  try {
    const userId = req.params.id;

    const user = await User.findById(userId);

    if (!user) {
      return res.status(400).json({
        success: false,
        msg: "User Does not exists...",
      });
    }

    const ordersByUser = await Order.find({ userid: userId });

    if (!ordersByUser) {
      return res.status(400).json({
        success: false,
        msg: "No Orders...",
      });
    }

    const count = ordersByUser.length;

    res.status(200).json({
      success: true,
      total: count,
      ordersByUser,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      msg: error.message,
    });
  }
};
